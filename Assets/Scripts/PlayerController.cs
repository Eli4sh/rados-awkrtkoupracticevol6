﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public CoinSpawnController coinSpawnScript;

    public Text scoreText;
    public GameObject spawnPoint;
    public GameObject bullet;
    public GameObject bulletHolder;
    public GameObject camera;
    public LayerMask Ground;

    private float moveSpeed = 160;
    private float jumpSpeed = 200;
    private float fireSpeed = 10;
    private int score = 0;
    bool facingLeft = false;
    bool isGrounded;

    private Rigidbody2D rb;
	
    void Awake()
    {
        transform.position = spawnPoint.transform.position;
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {

        isGrounded = Physics2D.IsTouchingLayers(gameObject.GetComponent<Collider2D>(), Ground);

        if (Input.GetKey("a"))
        {
            rb.AddForce(new Vector2(-moveSpeed, 0));
            facingLeft = true;

        }
        else if (Input.GetKey("d"))
        {
            rb.AddForce(new Vector2(moveSpeed, 0));
            facingLeft = false;
        }
        if (Input.GetKeyDown("w") && isGrounded) rb.AddForce(new Vector2(0, jumpSpeed));
        rb.velocity = new Vector2(0, rb.velocity.y);

        scoreText.text = "Your score: " + score.ToString();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject tempBullet = Instantiate(bullet, bulletHolder.transform.position, bulletHolder.transform.rotation) as GameObject;
            if (facingLeft == true) tempBullet.GetComponent<Rigidbody2D>().velocity = new Vector2(-fireSpeed, 0);
            else tempBullet.GetComponent<Rigidbody2D>().velocity = new Vector2(fireSpeed, 0);

            
        }

        
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name == "Pit")
            gameObject.transform.position = spawnPoint.transform.position;
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "Coin")
        {
            score++;
            coinSpawnScript.coinList.Remove(collision.gameObject);
            Destroy(collision.gameObject);
        }
    }
}
