﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

   public void WhichOption(string message)
    {
        switch (message)
        {
            case "start":
                SceneManager.LoadScene("Scene2", LoadSceneMode.Single);
                break;

            case "quit":
                Application.Quit();
                break;
                
        }
    }
}
