﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class CoinSpawnController : MonoBehaviour {

    public GameObject Coin;
    public List<GameObject> coinList;
    public List<GameObject> poolList;
    private float poolAmount = 20;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < poolAmount; i++)
        {

            GameObject tempCoin = Instantiate(Coin, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
            poolList.Add(tempCoin);
            tempCoin.gameObject.SetActive(false);
            
        }

	}
	
	// Update is called once per frame
	void Update () {

        if (coinList.Count <= 0) SceneManager.LoadScene("Scene1", LoadSceneMode.Single);
       // Vector3 coinSpawnPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f));

        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 coinSpawnPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f));
            GameObject TempCoin = Instantiate(Coin, new Vector3(coinSpawnPosition.x, coinSpawnPosition.y, 0.0f), Quaternion.identity) as GameObject;
            coinList.Add(TempCoin); 

          /*  for(int i = 0; i < poolList.Count; i++)
            {
                if (!poolList[i].activeInHierarchy)
                {
                    poolList[i].transform.position = coinSpawnPosition;
                    poolList[i].SetActive(true);
                    coinList.Add(poolList[i]);
                    break;
                    
                }
                else
                {
                    GameObject TempCoin = Instantiate(Coin, coinSpawnPosition, Quaternion.identity) as GameObject;
                    coinList.Add(TempCoin);
                } 
             } */
            
        }
       
	
	}
}
